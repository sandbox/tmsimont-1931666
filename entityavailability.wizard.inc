<?php


/**
 * Clears the wizard cache.
 *
 * @param integer $id
 *   cache id.
 */
function entityavailability_cache_clear($id) {
  ctools_include('object-cache');
  ctools_object_cache_clear('entityavailability', $id);
}
 
/**
 * Stores our little cache so that we can retain data from form to form.
 *
 * @param integer $id
 *   cache id.
 * @param object $object
 *   object with form values.
 */
function entityavailability_cache_set($id, $restriction) {
  ctools_include('object-cache');
  ctools_object_cache_set('entityavailability', $id, $restriction);
}
 
/**
 * Gets the current restriction from the cache, or default.
 *
 * @param integer $id
 *   cache id.
 *
 * @return restriction
 *   cache with stored stuff.
 */
function entityavailability_cache_get($id) {
  ctools_include('object-cache');
  $restriction = ctools_object_cache_get('entityavailability', $id);
  if (!$restriction) {
    // Create a default object.
    $restriction = new stdClass;
  }
 
  return $restriction;
}



/**
 * Step 1: get the entity that will be restricted
 */
function entityavailability_add_restricted_entity($form, &$form_state) {
  $entities = array();
  foreach (entity_get_info() as $this_entity_name => $info) {
    $entities[$this_entity_name] = isset($info['label'])?$info['label']:$this_entity_name;
  }
  $form['restricted_entity_name'] = array(
    '#type' => 'select',
    '#options' => $entities,
  );
  return $form;
}
 
/**
 * Step 1 submit: store the entity type selection into cache
 */
function entityavailability_add_restricted_entity_submit($form, &$form_state) {
  $form_state['restriction']->restricted_entity_name = $form_state['values']['restricted_entity_name'];
}

/**
 * Step 2: get the entity bundle that will be restricted
 */
function entityavailability_add_restricted_bundle($form, &$form_state) {
  $restricted_entity_name = $form_state['restriction']->restricted_entity_name;
  $entities = array();
  $info = entity_get_info($restricted_entity_name);
  $bundles = array();
  foreach ($info['bundles'] as $bundle => $data) {
    $bundles[$bundle] = $data['label'];
  }
  $form['restricted_entity_bundle'] = array(
    '#type' => 'select',
    '#options' => $bundles,
  );
  return $form;
}
 
/**
 * Step 2 submit: store the entity bundle selection into cache
 */
function entityavailability_add_restricted_bundle_submit($form, &$form_state) {
  $form_state['restriction']->restricted_entity_bundle = $form_state['values']['restricted_entity_bundle'];
}


/**
 * Step 3: get the entityavailability_add_restricting_date that will be restricted
 */
function entityavailability_add_restricting_date($form, &$form_state) {
  $this_entity_name = $form_state['restriction']->restricted_entity_name;
  $this_bundle = $form_state['restriction']->restricted_entity_bundle;
  
  // pull in all of the date and entity reference fields on the site
  $date_fields = _entityavailability_get_date_fields($this_entity_name);
  $entityreference_fields = _entityavailability_get_reference_fields($this_entity_name);
  
  // remove bundles from date options that don't have references to this bundle
  foreach ($date_fields as $entity => $bundles) {
    if (!isset($entityreference_fields[$entity]) && $entity != $this_entity_name) {
      unset($date_fields[$entity]);
    }
    foreach ($bundles as $bundle => $fields) {
      if (!isset($entityreference_fields[$entity][$bundle]) && $bundle != $this_bundle) {
        unset($date_fields[$entity][$bundle]);
      }
    }
  }
  
  // build options based on entities with date fields that have reference fields pointing to this entity
  $date_options = array();
  $total_opts = 0;
  foreach ($date_fields as $entity => $bundles) {
    foreach ($bundles as $bundle => $fields) {
      foreach ($fields as $field_name => $info) {
        if (isset($entityreference_fields[$entity]) && isset($entityreference_fields[$entity][$bundle])) {
          foreach ($entityreference_fields[$entity][$bundle] as $rfield_name => $rfield) {
            $date_options[$bundle . " " . $entity . " entities referencing available entity via " . $rfield_name][$entity . ":" . $bundle . ":" . $field_name . ":" . $rfield_name] = $field_name;
            $total_opts++;
          }
        }
      }
    }
  }
  
  // add in any date fields on "this bundle" as an option for self-contained availability date fields
  if (isset($date_fields[$this_entity_name][$this_bundle])) {
    foreach ($date_fields[$this_entity_name][$this_bundle] as $field_name => $field) {
      $date_options['this ' . $this_entity_name . " " . $this_bundle][$this_entity_name . ":" . $this_bundle . ":" . $field_name . ":#this"] = $field_name;
    }
  }
  
  

  // sort the optgroups alphabetically
  ksort($date_options);

  $form['help'] = array(
    "#markup" => "<p>" . t("These lists of fields include date fields that are attached to the selected Available Entity/Bundle as well as date fields that can be connected to the Available Entity/Bundle via an Entity Reference field.") . "</p>",
  );
  $form['restricting_dates'] = array(
    '#title' => t("Date fields that negate availability"),
    '#type' => 'select',
    '#multiple' => true,
    '#size' => $total_opts<18?count($date_options)+$total_opts+1:20,
    '#options' => $date_options,
    '#description' => t("The entity is assumed available unless described as unavailable within a time period defined by one of these fields."),
  );
  
  return $form;
}
/**
 * Step 3 submit: store the entity bundle selection into cache
 */
function entityavailability_add_restricting_date_submit($form, &$form_state) {
  $form_state['restriction']->restricting_dates = $form_state['values']['restricting_dates'];
}
 
/**
 * Step 4: store the entity bundle selection into cache
 */
function entityavailability_add_referrer_to_restrict($form, &$form_state) {
  $this_entity_name = $form_state['restriction']->restricted_entity_name;
  $this_bundle = $form_state['restriction']->restricted_entity_bundle;
  
  
  // pull in all of the date and entity reference fields on the site
  $date_fields = _entityavailability_get_date_fields($this_entity_name);
  $entityreference_fields = _entityavailability_get_reference_fields($this_entity_name);
  
  // build a list of entity reference options
  $entityreference_options = array();
  $total_opts = 0;
  foreach ($entityreference_fields as $entity => $bundles) {
    if ($entity == 'all') {
      foreach ($bundles as $field_name => $info) {
        foreach ($date_fields['all'] as $dfield_name => $dfield) {
          $entityreference_options['on all entities']['all:all:' . $field_name . ':' . $dfield_name] = $field_name . ' within range of ' . $dfield_name;
          $total_opts++;
        }
      }
    } else {
      foreach ($bundles as $bundle => $fields) {
        foreach ($fields as $field_name => $info) {
          if (isset($date_fields[$entity]) && isset($date_fields[$entity][$bundle])) {
            foreach ($date_fields[$entity][$bundle] as $dfield_name => $dfield) {
              $entityreference_options['on ' . $entity . ": " . $bundle][$entity . ":" . $bundle . ":" . $field_name . ':' . $dfield_name] = $field_name . ' within range of ' . $dfield_name;
              $total_opts++;
            }
          }
        }
      }
    }
  }

  // build options based on entities with date fields that have reference fields pointing to this entity
  $date_options = array();
  $total_opts = 0;
  foreach ($date_fields as $entity => $bundles) {
    foreach ($bundles as $bundle => $fields) {
      foreach ($fields as $field_name => $info) {
        if (isset($entityreference_fields[$entity]) && isset($entityreference_fields[$entity][$bundle])) {
          foreach ($entityreference_fields[$entity][$bundle] as $rfield_name => $rfield) {
            $date_options[$entity . " " . $bundle . " referencing available entity via " . $rfield_name][$entity . ":" . $bundle . ":" . $field_name . ":" . $rfield_name] = $field_name;
            $total_opts++;
          }
        }
      }
    }
  }
  
  // add in any date fields on "this bundle" as an option for self-contained availability date fields
  if (isset($date_fields[$this_entity_name][$this_bundle])) {
    foreach ($date_fields[$this_entity_name][$this_bundle] as $field_name => $field) {
      $date_options['this ' . $this_entity_name . " " . $this_bundle][$this_entity_name . ":" . $this_bundle . ":" . $field_name . ":#this"] = $field_name;
    }
  }  
  
  $form['referrers_to_restrict'] = array(
    '#title' => t("Reference fields to restrict when this entity is not available"),
    '#type' => 'select',
    '#multiple' => true,
    '#size' => $total_opts<18?count($entityreference_options)+$total_opts+1:20,
    '#options' => $entityreference_options,
    '#description' => t('These fields reference the selected Available Entity/Bundle.  Indicate which of these fields are restricted based on the availability of this entity.'),
  );
  return $form;
}
/**
 * Step 4 submit: store the entity bundle selection into cache
 */
function entityavailability_add_referrer_to_restrict_submit($form, &$form_state) {
  $form_state['restriction']->referrers_to_restrict = $form_state['values']['referrers_to_restrict'];
}


function entityavailability_add_steps_next(&$form_state) {
  entityavailability_cache_set($form_state['restriction_id'], $form_state['restriction']);
}
function entityavailability_add_steps_finish(&$form_state) {

  // info about the entity whose availability is in question
  $entity_info = array(
    'restricted_entity_name' => $form_state['restriction']->restricted_entity_name,
    'restricted_entity_bundle' => $form_state['restriction']->restricted_entity_bundle,
  );
  
  // date fields that define periods of restriction
  $restricting_dates = array();
  $restricting_dates_input = $form_state['restriction']->restricting_dates;
  foreach ($restricting_dates_input as $value) {
    $parts = explode(":", $value);
    $restricting_dates[] = array(
      'restricting_date_entity' => $parts[0],
      'restricting_date_bundle' => $parts[1],
      'restricting_date_field' => $parts[2],
      'restricting_date_ref_to_target' => $parts[3],
    );
  }
  
  // reference fields to restrict
  $reference_fields = array();
  $reference_fields_input = $form_state['restriction']->referrers_to_restrict;
  foreach ($reference_fields_input as $value) {
    $parts = explode(":", $value);
    $reference_fields[] = array(
      'referrer_to_restrict_entity' => $parts[0],
      'referrer_to_restrict_bundle' => $parts[1],
      'referrer_to_restrict_entityreference' => $parts[2],
      'referrer_to_restrict_date' => $parts[3],
    );
  }
  
  // put it all together  
  $saved = 0;
  foreach ($restricting_dates as $restrictor) {
    foreach ($reference_fields as $restricted) {
      if (_entityavailability_insert_restriction( $entity_info + $restrictor + $restricted)) {
        $saved++;
      }
    }
  }
  
  drupal_set_message("$saved new entity availability rule(s) saved.");
  
  $form_state['redirect'] = 'admin/structure/entityavailability';
}
function entityavailability_add_steps_cancel(&$form_state) {
  $form_state['redirect'] = 'admin/structure/entityavailability';
}