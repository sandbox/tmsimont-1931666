<?php

/**
 * @file
 * Hooks provided by the Entity Availability module
 *
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Handle conflicts when an entity has referenced an entity that is unavailable.
 *
 * @param $conflicts array
 *  This is an array that contains a list of conflicts that were found when
 *  entityavailability_check_availability was called.  Each conflict is an array
 *  of data that will help make sense of the overlap.
 *  @see entityavailability_check_availability()
 *
 * @param $form_state array
 *  This is set to a Drupal entity form form_state when the function is called from
 *  a validation handler.  If set, form_set_error will trigger an error on the entity
 *  form.  The reference field and date field are both found in the $conflicts array.
 */
function hook_entityavailability_handle_conflict(&$conflicts, &$form_state = null) {
  foreach($conflicts as $idx => $conflict) {
    //gather this referrer information
    $referrer_type = $conflict->referring_entity_type;
    $referrer = $conflict->referring_entity;
    
    //gather "available entity" information
    $available_entity_type = $conflict->available_entity_type;
    $available_entity = $conflict->available_entity;
    
    //gather conflicting referrer information
    $conflicter_entity_type = $conflict->conflicting_entity_type;
    $conflicter_entity = $conflict->conflicting_entity;
    
    if (!isset($referrer->is_new) && $referrer->identifier() == $available_entity->identifier() && $referrer_type == $available_entity_type) {
      // the referrer and the available entity are the same.
    }
    
    if (!isset($referrer->is_new) && $referrer->identifier() == $conflicter_entity->identifier() && $referrer_type == $conflicter_entity_type) {
      // the referrer and the conflicting referrer are the same entity.
    }
    
    if ($referrer->uid == $available_entity->uid) {
        // the referrer user owns the available entity.
    }
    
    if ($referrer->uid == $conflicter_entity->uid) {
        // the referrer user owns the conflicting entity.
    }
    
    // if this is form validation, set error
    if ($form_state) {
      form_set_error($conflict->getReferrerReferenceFieldName(), "This entity is not available during the selected time");
    }
    
    // unset this because it's been addressed
    unset($conflicts[$idx]);
  }
}

/**
 * Allows a module to alter the dates that get passed into 
 * entityavailability_check_availability() during entity form
 * validation.
 *
 * @param $dates_to_check
 *  an array of dates to check against an entity's availability.  the
 *  format of this should match the format of a date field value (without
 *  the language array key)
 *
 * @param $context
 *  provides additional context to the alter process.
 *  $context['rule'] is the Entity Availability restriction rule definition.
 *  $context['referrer_type'] is the type of entity trying to reference the available entity
 *  $context['referrer'] is the loaded entity object trying to reference the available entity
 *  $context['form'] is the form that was submitted when the validation rule was fired
 *  $context['form_state'] is the form state of the submitted form
 */
function hook_entityavailability_availability_dates_alter(&$dates_to_check, $context) {
  //example from Date Set Management module
  $rule = $context['rule'];
  $form = $context['form'];
  $form_state = $context['form_state'];

  if ($rule['referrer_to_restrict_date'] == 'dsm_dates' 
    && ( $rule['referrer_to_restrict_entity'] == 'dateset'
         || $rule['referrer_to_restrict_entity'] == 'all' )
    && $context['referrer_type'] == 'dateset') {
    $dateset = $context['referrer'];
    entity_form_submit_build_entity('dateset', $dateset, $form, $form_state);
    $dateset->prepareDateField();
    $dates_to_check = $dateset->dsm_dates[LANGUAGE_NONE];
  }
}

/**
 * @} End of "addtogroup hooks".
 */