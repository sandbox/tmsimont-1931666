<?php

/**
 * Implements hook_rules_data_info().
 * Define restriction rule property info.
 *
 * @ingroup rules
 */
function entityavailability_rules_data_info() {
  $dummy_conflict = new EntityAvailabilityConflict();
  return array(
    'entityavailability_conflict' => array(
      'label' => t("availability conflict"),
      'description' => t("Data related to an availability clash between specific entities."),
      'property info' => $dummy_conflict->propertyInfo(),
    ),
    'restriction_rule' => array(
      'label' => t("restriction rule"),
      'description' => t("The rule that defines the availability restrictions imposed upon non-specific entities."),
      'property info' => array(
        'id' => array(
          'type' => 'integer',
          'label' => 'id',
          'description' => t("The database primary key for this restriction rule."),
        ),
        'restricted_entity_name' => array(
          'type' => 'text',
          'label' => t("Available entity name"),
          'description' => t("Entity type name that is availabile/unavailable."),
        ),
        'restricted_entity_bundle' => array(
          'type' => 'text',
          'label' => t("Available bundle name"),
          'description' => t("Entity bundle that is availabile/unavailable."),
        ),
        'restricting_date_entity' => array(
          'type' => 'text',
          'label' => t("Restricing date entity"),
          'description' => t("Entity that has set a restriction reference."),
        ),
        'restricting_date_bundle' => array(
          'type' => 'text',
          'label' => t("Restricting date bundle"),
          'description' => t("Entity that has set a restricting date range."),
        ),
        'restricting_date_field' => array(
          'type' => 'text',
          'label' => t("Restricting date field"),
          'description' => t("The field that defines the restricting date range."),
        ),
        'restricting_date_ref_to_target' => array(
          'type' => 'text',
          'label' => t("Restricting date reference to target"),
          'description' => t("The field that references the target entity from the restrictor."),
        ),
        'referrer_to_restrict_entity' => array(
          'type' => 'text',
          'label' => t("Referrer to restrict entity"),
          'description' => t("The entity name of the restricted referring entity."),
        ),
        'referrer_to_restrict_bundle' => array(
          'type' => 'text',
          'label' => t("Referrer to restrict bundle"),
          'description' => t("The bundle name of the restricted referring entity."),
        ),
        'referrer_to_restrict_entityreference' => array(
          'type' => 'text',
          'label' => t("Referrer to restrict entity reference field"),
          'description' => t("The entity reference field on the restricted referring entity."),
        ),
        'referrer_to_restrict_date' => array(
          'type' => 'text',
          'label' => t("Referrer to restrict date field"),
          'description' => t("The date range definition field that on the restricted referring entity."),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 * Define availability conflict as an event.
 *
 * @ingroup rules
 */
function entityavailability_rules_event_info() {
  $items = array(
    'entityavailability_conflict' => array(
      'label' => t('A user tries to reference a restricted entity'), 
      'group' => t('Entity Availability'), 
      'access callback' => 'administer entityavailability',
      'help' => t('Tell the system how to handle Entity Availability conflicts'),
      'variables' => array(
        'active_rule' => array(
          'label' => t("Active restriction rule"),
          'type' => 'restriction_rule',
          'descripiton' => t("The rule that defines the availabiltiy of the target entity that is actively in conflict."),
        ),
        'active_conflict' => array(
          'label' => t("Active availability conflict data"),
          'type' => 'entityavailability_conflict',
          'descripiton' => t("Data associated with this specific availability conflict."),
        ),
        'referring_entity_user' => array(
          'label' => t("UID owner of restricted referring entity type"),
          'type' => 'user',
          'description' => t("User that owns the referrer that has encountered a conflict with a reference."),
        ),
        'available_entity_user' => array(
          'label' => t("UID owner of available entity type"),
          'type' => 'user',
          'description' => t("User that owns the target reference that has availbility restrictions."),
        ),
        'conflicting_referrer_user' => array(
          'label' => t("UID owner of conflicting referrer type"),
          'description' => t("User that owns the conflicting referrer that has imposed restrictions upon the target entity."),
          'type' => 'user',
        ),
        'referring_entity_type' => array(
          'label' => t("Restricted referring entity type"),
          'type' => 'text',
          'description' => t("Entity type of the referrer that has encountered a conflict with a reference."),
        ),
        'available_entity_type' => array(
          'label' => t("Available entity type"),
          'type' => 'text',
          'description' => t("Entity type of the target reference that has availbility restrictions."),
        ),
        'conflicting_referrer_type' => array(
          'label' => t("Conflicting referrer type"),
          'description' => t("Entity type of the conflicting referrer that has imposed restrictions upon the target entity."),
          'type' => 'text',
        ),
        'referring_entity' => array(
          'label' => t("Restricted referring entity"),
          'type' => 'entity',
          'description' => t("The referrer that has encountered a conflict with a reference."),
        ),
        'available_entity' => array(
          'label' => t("Available entity"),
          'type' => 'entity',
          'description' => t("The target reference that has availbility restrictions."),
        ),
        'conflicting_referrer' => array(
          'label' => t("Conflicting referrer"),
          'description' => t("The conflicting referrer that has imposed restrictions upon the target entity."),
          'type' => 'entity',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_rules_condition_info().
 */
function entityavailability_rules_condition_info() {
  return array(
    'entityavailability_entity_relationship_conditions_group' => array(
      'label' => t('Entity availability conditions'), 
      'parameter' => array(
        'active_conflict' => array(
          'label' => t("Active availability conflict data"),
          'type' => 'entityavailability_conflict',
          'descripiton' => t("Data associated with this specific availability conflict."),
        ),
        'type' => array(
          'type' => 'list<text>',
          'label' => t("Action allowed for conflict resolution"),
          'options list' => 'entityavailabiltiy_conflict_resolution_types',
        ),
        'relationship_to_conflict' => array(
          'type' => 'list<text>',
          'label' => t("Relationship between referrer and conflict"),
          'options list' => 'entityavailabiltiy_relationship_types',
        ),
        'relationship_to_available_entitiy' => array(
          'type' => 'list<text>',
          'label' => t("Relationship between referrer and available entity"),
          'options list' => 'entityavailabiltiy_relationship_types',
        ),
        'approval' => array(
          'type' => 'boolean',
          'label' => t("Approval is required"),
        ),
      ), 
      'group' => t('Entity availability'),
    ),
  );
}

/**
 * callback for entity availability conditions group handling
 */
function entityavailability_entity_relationship_conditions_group($conflict, $type, $ref_to_conflict, $ref_to_available_entity, $approval) {
  // make sure relationship conditions requirements match
  if (entityavailability_entity_relationship_conditions_group_match_conditions(
    $conflict->referrerToConflictingEntityRelationships(), 
    $ref_to_conflict
  )) {
    return true;
  }
  if (entityavailability_entity_relationship_conditions_group_match_conditions(
    $conflict->referrerToAvailableEntityRelationships(), 
    $ref_to_available_entity
  )) {
    return true;
  }
  
  //TODO: pull settings out of {entityavailability_resolution_settings} and compare against arguments and conflict entities
  
  return true;
}

/**
 * helper to match actual relationships against required
 * @see entityavailability_entity_relationship()
 */
function entityavailability_entity_relationship_conditions_group_match_conditions($actual, $required) {
  //TODO: allow "any" vs. "all" handling... currently only using "any"
  foreach($required as $condition_to_match) {
    if(isset($actual[$condition_to_match]) && $actual[$condition_to_match]) {
      return true;
    }
    // handle special condition "value passed with ':' separating multiple parts
    $special_condition = explode(":", $condition_to_match);
    if (count($special_condition)>1) {
      //TODO: consider a module:fuction:param string conventions or something, maybe a module hook here
      if (module_exists("user_relationships") && $special_condition[0] == "user_relationship") {
        $rid = $special_condition[1];
        if (isset($actual['user_relationships'][$rid])) {
          return true;
        }
      }
    }
  }
}

/**
 * Implements hook_rules_action_info().
 *
 * @todo
 *  provide action that will present options the user:
 *   - allow immediate action
 *   - request permission for action (if user relationship/private msg exist?)
 *   - display "you can't do anything" messaging
 * 
 * @ingroup rules
 */
function entityavailability_rules_action_info() {
  $parameter = array(
    'active_rule' => array(
      'type' => 'restriction_rule',
      'label' => t('Active Rule'),
    ),
    'active_confict' => array(
      'type' => 'entityavailability_conflict',
      'label' => t('Active Conflict'),
    ),
  );
  return array(
    'entityavailability_adjust_conflict' => array(
      'label' => t('Adjust conflict'), 
      'parameter' => $parameter, 
      'group' => t('Entity Availability'),
    ),
    'entityavailability_request_change' => array(
      'label' => t('Request update'), 
      'parameter' => $parameter,
      'group' => t('Entity Availability'),
    ),
    'entityavailability_validation_error' => array(
      'label' => t('Show validation error'), 
      'parameter' => $parameter,
      'group' => t('Entity Availability'),
    ),
  );
}

/**
 * - adjust conflicting entity
 * - remove conflicting entity
 * - allow both to exist (make record of double booking?)
 * - share ownership of conflict
 */
function entityavailability_resolution($restriction_rule, $conflict) {
  //@todo: stash form state somehow from conflict and and then show form to handle resolution
}

function entityavailability_adjust_conflict($restriction_rule, $conflict) {
  // entityavailability_resolution($restriction_rule, $conflict);
  $resolution = new EntityAvailabilityResolutionShareOwnership($restriction_rule, $conflict);
  $conflict->addResolutionOption($resolution);
}

function entityavailability_request_change($restriction_rule, $conflict) {
  $resolution = new EntityAvailabilityResolution($restriction_rule, $conflict);
  $conflict->addResolutionOption($resolution);
}

function entityavailability_validation_error($restriction_rule, $conflict) {
  form_set_error($conflict->getReferrerReferenceFieldName(), $conflict->formErrorMessage());
}