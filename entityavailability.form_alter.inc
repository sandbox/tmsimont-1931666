<?php
/**
 * @file 
 *
 * This was an approach that puts the Entity Availability settings form onto the _type_form forms.
 * I gave up because it was getting too confusing (too many fields) and the removal/update process
 * was too difficult to work out in a constricted environment.
 * The ctools multistep wizard solved a lot of my problems, so i moved this out to several settings
 * pages under /admin/structure
 *
 */



/**
 * Implements hook_form_alter().
 */
function _entityavailability_form_alter(&$form, &$form_state, $form_id) {
  // alter all [entity name]_type_form forms
  foreach (entity_get_info() as $this_entity_name => $info) {
    if ($form_id == $this_entity_name . "_type_form") {
    
      // try to determine the "this bundle" name
      $this_bundle = false;
      if (isset($form['type']) && isset($form['type']['#default_value']) && !empty($form['type']['#default_value'])) {
        if (in_array($form['type']['#default_value'], array_keys($info['bundles']))) {
          // "type" value is set into form (nodes and profile2 do this)
          $this_bundle = $form['type']['#default_value'];
        }
      } else {
        $args = arg();
        foreach ($args as $arg) {
          if (in_array($arg, array_keys($info['bundles']))) {
            // a valid bundle name is in the path args... could be a bad idea to assume thats what we're building, but what else can you do?
            $this_bundle = $arg;
          }
        }
      }
      
      // unable to determine "this bundle" so don't show anything here
      if ($this_bundle === false) {
        return false;
      }
      
      // initialize default values for entityavailability fields
      $default_values = array(
        'restricting_dates' => array(),
        'referrers_to_restrict' => array(),
      );
      
      // transalate saved restrictions into default values for this form
      $saved_restrictions = _entityavailability_get_restrictions_assoc();
      if (isset($saved_restrictions[$this_entity_name]) && isset($saved_restrictions[$this_entity_name][$this_bundle])) {
        $this_restrictions = $saved_restrictions[$this_entity_name][$this_bundle];
        foreach ($this_restrictions['restricting_dates'] as $entity => $bundles) {
          foreach ($bundles as $bundle => $fields) {
            foreach ($fields as $field_name => $restrictions) {
              foreach ($restrictions as $restriction) {
                $rfield_name = $restriction['restricting_date_ref_to_target'];
                $default_values['restricting_dates'][$entity . ":" . $bundle . ":" . $field_name . ":" . $rfield_name] = $field_name;
              }
            }
          }
        }
        foreach ($this_restrictions['referrers_to_restrict'] as $entity => $bundles) {
          foreach ($bundles as $bundle => $fields) {
            foreach ($fields as $field_name => $restrictions) {
              foreach ($restrictions as $restriction) {
                $default_values['referrers_to_restrict'][$entity . ":" . $bundle . ":" . $field_name] = $field_name;
              }
            }
          }
        }
      }

      // create a fieldset on the entity_type_form for the Entity Availability settings
      $label = $info['label'];
      $form['entityavailability'] = array(
        '#title' => "Entity Availability",
        '#type' => 'fieldset',
        '#group' => 'additional_settings',
        '#collapsible' => true,
        '#collapsed' => true,
        '#description' => t("Configure time-sensitive restrictions on Entity Reference fields pointing to this bundle of the %label entity type.", array('%label' => $label)),
      );
      
      // pull in all of the date and entity reference fields on the site
      $date_fields = _entityavailability_get_date_fields($this_entity_name);
      $entityreference_fields = _entityavailability_get_reference_fields($this_entity_name);
      
      // remove bundles from date options that don't have references to this bundle
      foreach ($date_fields as $entity => $bundles) {
        if (!isset($entityreference_fields[$entity]) && $entity != $this_entity_name) {
          unset($date_fields[$entity]);
        }
        foreach ($bundles as $bundle => $fields) {
          if (!isset($entityreference_fields[$entity][$bundle]) && $bundle != $this_bundle) {
            unset($date_fields[$entity][$bundle]);
          }
        }
      }
      
      // build options based on entities with date fields that have reference fields pointing to this entity
      $date_options = array();
      foreach ($date_fields as $entity => $bundles) {
        foreach ($bundles as $bundle => $fields) {
          foreach ($fields as $field_name => $info) {
            if (isset($entityreference_fields[$entity]) && isset($entityreference_fields[$entity][$bundle])) {
              foreach ($entityreference_fields[$entity][$bundle] as $rfield_name => $rfield) {
                $date_options[$entity . " " . $bundle . " via " . $rfield_name][$entity . ":" . $bundle . ":" . $field_name . ":" . $rfield_name] = $field_name;
              }
            }
          }
        }
      }
      
      // add in any date fields on "this bundle" as an option for self-contained availability date fields
      if (isset($date_fields[$this_entity_name][$this_bundle])) {
        foreach ($date_fields[$this_entity_name][$this_bundle] as $field_name => $field) {
          $date_options['this ' . $this_entity_name . " " . $this_bundle][$this_entity_name . ":" . $this_bundle . ":" . $field_name . ":#this"] = $field_name;
        }
      }
      
      // build a list of entity reference options
      $entityreference_options = array();
      foreach ($entityreference_fields as $entity => $bundles) {
        if ($entity == 'all entities') {
          foreach ($bundles as $field_name => $info) {
            $entityreference_options['all entities']['all:all:'.$field_name] = $field_name;
          }
        } else {
          foreach ($bundles as $bundle => $fields) {
            foreach ($fields as $field_name => $info) {
              $entityreference_options[$entity . ": " . $bundle][$entity . ":" . $bundle . ":" . $field_name] = $field_name;
            }
          }
        }
      }
      
      // sort the optgroups alphabetically
      ksort($date_options);
      
      // attach fields to form
      $form['entityavailability']['restricting_dates'] = array(
        '#title' => t("Date fields that negate availability."),
        '#type' => 'select',
        '#multiple' => true,
        '#size' => count($date_options)<18?count($date_options)+5:20,
        '#options' => $date_options,
        '#default_value' => array_keys($default_values['restricting_dates']),
        '#description' => t("The entity is assumed available unless described as unavailable within a time period defined by one of these fields.  Fields that are attached to the selected Available Entity/Bundle selection, and fields that can be connected to the Available Entity/Bundle via Entity Reference field."),
      );
      
      $form['entityavailability']['referrers_to_restrict'] = array(
        '#title' => t("Reference fields to restrict when date is set"),
        '#type' => 'select',
        '#multiple' => true,
        '#options' => $entityreference_options,
        '#default_value' => array_keys($default_values['referrers_to_restrict']),
        '#description' => t('These fields reference this bundle.  Indicate which of these fields are restricted based on the availability of this entity.'),
      );
      
      $form['entityavailability']['restricted_entity_name'] = array( 
        '#type' => 'hidden',
        '#value' => $this_entity_name,
      );
      $form['entityavailability']['restricted_entity_bundle'] = array( 
        '#type' => 'hidden',
        '#value' => $this_bundle,
      );
      
      $form['#validate'][] = '_entityavailability_form_validate';
      $form['#submit'][] = '_entityavailability_form_submit';
      
    }
  }
}

/**
 * validation callback for Entity Availability form attachment
 * @todo: what kind of validation is required?
 */
function _entityavailability_form_validate(&$form, &$form_state) {
}


/**
 * return an associative array of all of the entityavailability restriction definitions
 * @return array()
 *  [restricted entity]
 *    [restrcited bundle]
 *      [restricting_dates]
 *        [entity]
 *          [bundle]
 *            [fields]
 *              [complete restriction rule record]
 *      [referrers_to_restrict]
 *        [entity]
 *          [bundle]
 *            [fields]
 *              [complete restriction rule record]
 */
function _entityavailability_get_restrictions_assoc() {
  // @todo: implement drupal static
  $restrictions = array();
  foreach (_entityavailability_get_restrictions() as $row) {
    $restrictions[$row['restricted_entity_name']][$row['restricted_entity_bundle']]['restricting_dates'][$row['restricting_date_entity']][$row['restricting_date_bundle']][$row['restricting_date_field']][] = $row;
    $restrictions[$row['restricted_entity_name']][$row['restricted_entity_bundle']]['referrers_to_restrict'][$row['referrer_to_restrict_entity']][$row['referrer_to_restrict_bundle']][$row['referrer_to_restrict_entityreference']][] = $row;
  }
  return $restrictions;
}

/**
 * submit callback for Entity Availability form attachment
 */
function _entityavailability_form_submit(&$form, &$form_state) {
  
  // info about the entity whose availability is in question
  $entity_info = array(
    'restricted_entity_name' => $form_state['values']['restricted_entity_name'],
    'restricted_entity_bundle' => $form_state['values']['restricted_entity_bundle'],
  );
  
  // date fields that define periods of restriction
  $date_fields = array();
  $date_fields_input = $form_state['values']['restricting_dates'];
  foreach ($date_fields_input as $value) {
    $parts = explode(":", $value);
    $date_fields[] = array(
      'restricting_date_entity' => $parts[0],
      'restricting_date_bundle' => $parts[1],
      'restricting_date_field' => $parts[2],
      'restricting_date_ref_to_target' => $parts[3],
    );
  }
  
  // reference fields to restrict
  $reference_fields = array();
  $reference_fields_input = $form_state['values']['referrers_to_restrict'];
  foreach ($reference_fields_input as $value) {
    $parts = explode(":", $value);
    $reference_fields[] = array(
      'referrer_to_restrict_entity' => $parts[0],
      'referrer_to_restrict_bundle' => $parts[1],
      'referrer_to_restrict_field' => $parts[2],
    );
  }
  
  // put it all together  
  foreach ($date_fields as $restrictor) {
    foreach ($reference_fields as $restricted) {
      _entityavailability_insert_restriction( $entity_info + $restrictor + $restricted);
    }
  } 
}