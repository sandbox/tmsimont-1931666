<?php


/**
 * Form callback for adding a new availability rule
 */
function entityavailability_add($step = NULL) {

  module_load_include("inc", "entityavailability", "entityavailability.wizard");

  //@todo: better object cache id handling?
  $restriction_id = count(_entityavailability_get_restrictions()) + 1;
  
  if (arg(4)) {
    $step = arg(4);
  } else {
    entityavailability_cache_clear($restriction_id);
    $step = 'restricted_entity';
  }
  
  // This automatically gets defaults if there wasn't anything saved.
  $restriction = entityavailability_cache_get($restriction_id);
  
  $form_info = array(
    'id' => 'entityavailability_add_steps', 
    'path' => "admin/structure/entityavailability/add/%step", 
    'show trail' => TRUE, 
    'show back' => TRUE, 
    'show cancel' => TRUE, 
    'show return' => FALSE, 
    'next callback' =>  'entityavailability_add_steps_next', 
    'finish callback' => 'entityavailability_add_steps_finish', 
    'cancel callback' => 'entityavailability_add_steps_cancel', 
    'order' => array( 
      'restricted_entity' => t('Available Entity'),
      'restricted_bundle' => t('Available Entity Bundle'),
      'restricting_date' => t('Restricting Date Field'),
      'referrer_to_restrict' => t('Restricted Entity Reference Field'),
    ),
    'forms' => array(
      'restricted_entity' => array(
        'form id' => 'entityavailability_add_restricted_entity'
      ),
      'restricted_bundle' => array(
        'form id' => 'entityavailability_add_restricted_bundle'
      ),
      'restricting_date' => array(
        'form id' => 'entityavailability_add_restricting_date'
      ),
      'referrer_to_restrict' => array(
        'form id' => 'entityavailability_add_referrer_to_restrict'
      ),
    ),
  );
 
  // live $form_state changes.
  $form_state = array(
    // Put our restriction and ID into the form state cache so we can easily find it.
    'restriction_id' => $restriction_id,
    'restriction' => &$restriction,
  );
 
  ctools_include('wizard');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);
  $output = drupal_render($form, $form_state);
  
  return $output;
}

function entityavailability_edit($form, &$form_state) {
  //@todo: allow editing of rules?
  /**
   * right now this will be difficult since each unique combination
   * of entity, bundle and field selections yields a unique record.
   *
   */
}

/**
 * delete form callback for delete page.
 */
function entityavailability_delete($form, &$form_state, $id) {
  $form['restriction_id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  return confirm_form($form, t("Are you sure you want to remove this rule?"), 'admin/structure/entityavailability');
}
function entityavailability_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/entityavailability';
  //delete record
  db_delete('entityavailability_restrictions')->condition('id', $form_state['values']['restriction_id'])->execute();
  drupal_set_message("The rule has been deleted");
  return;
}

/**
 * Page callback for entity availability overview
 */ 
function entityavailability_admin() {
  $header = array(
    array(
      'data' => 'Available Entity',
      'colspan' => 2,
    ),
    array(
      'data' => 'Restricting Date Field',
    ),
    array(
      'data' => 'Restricted Entity Reference Field',
    ),
    array(
      'data' => 'Operations',
      'colspan' => 2,
    ),
  );
  
  $rows = array();
  $result = db_query("SELECT * FROM {entityavailability_restrictions}");
  foreach (_entityavailability_get_restrictions() as $row) {
    $table_row = array(
      'availble-entity' => $row['restricted_entity_name'],
      'availble-bundle' => $row['restricted_entity_bundle'],
      'restricting-date' => entityavailability_describe_restricting_date($row),
      'restricted-reference' => entityavailability_describe_restricted_reference($row),
      //@see entityavailability_edit
      //'edit-link' => l('edit', 'admin/structure/entityavailability/'.$row['id']),
      'delete-link' => l('delete', 'admin/structure/entityavailability/'.$row['id'].'/delete'),
    );
    $rows[$row['id']] = array(
      'data' => $table_row,
    );
  }
  
  if (count($rows)==0) {
    return t("No availability rules have been created.");
  }
  
  return theme("table", array('header' => $header, 'rows' => $rows));
}