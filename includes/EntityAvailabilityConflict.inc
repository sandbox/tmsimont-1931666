<?php

/**
 * Helper to contain and describe data model related to entity availability conflicts.  
 * This helps pass data around to rules and other modules when a conflict is discovered.
 * This is not an entity, because it doesn't need fields and doesn't need to be saved...
 * @todo: consider making this an entity, or extend RulesIdentifiableDataWrapper
 */
class EntityAvailabilityConflict {
  protected $_valid;
  protected $_resolutions;
  
  public function propertyInfo() {
    return array(
      'conflict_type' => array(
        'type' => 'text',
        'label' => t("Conflict type."),
        'description' => t("This describes how the conflicting entities' date values overlap.  The overlap is either an exact overlap, or the timelines may be partially overlapped."),
      ),
      'referring_entity_date_value' => array(
        'type' => 'date',
        'label' => t("Referring entity date"),
        'description' => t("The referring entity is trying to reference the available entity in this date range."),
      ),
      'referring_entity_type' => array(
        'type' => 'text',
        'label' => t("Referring entity type"),
        'description' => t("This is the entity type of the referring entity that is being saved."),
      ),
      'referring_entity' =>  array(
        'type' => 'entity',
        'label' => t(""),
        'description' => t("This is the actual referring entity object that is being saved."),
      ),
      'referring_entity_reference_field' => array(
        'type' => 'text',
        'label' => t("Referring entity reference field"),
        'description' => t("This is the name of the entity reference field that the referring entity is using to reference the available entity."),
      ),
      'conflicting_range' => array(
        'type' => 'text',
        'label' => t("Conflicting dates"),
        'description' => t("This is the date range that has already been 'reserved.'"),
      ),
      'conflicting_entity_type' => array(
        'type' => 'text',
        'label' => t("Conflicting entity type"),
        'description' => t("This is the entity type of the conflicting entity referrer."),
      ),
      'conflicting_entity' => array(
        'type' => 'entity',
        'label' => t("Conflicting entity"),
        'description' => t("This is the actual conflicting entity object that has already been saved."),
      ),
      'available_entity_type' => array(
        'type' => 'text',
        'label' => t("Available entity type"),
        'description' => t("This is the entity type name of the available entity."),
      ),
      'available_entity' => array(
        'type' => 'entity',
        'label' => t("Available Entity"),
        'description' => t("This is the entity that is not available."),
      ),
    );
  }

  /**
   * construct a conflict object
   * @todo: valiate info array input
   */
  public function __construct($info = array()) {
    if (!isset($info['type'])) {
      return;
    }
    
    $this->_resolutions = array();
    
    $this->conflict_type = $info['type'];
    $this->rule = $info['rule'];
    
    $this->referring_entity_type = $info['referring_entity_type'];
    $this->referring_entity = $info['referring_entity'];
    $this->referring_entity_id = _entityavailability_get_entity_id($this->referring_entity_type, $this->referring_entity);
    
    $this->available_entity_type = $info['available_entity_type'];
    $this->available_entity = $info['available_entity'];
    
    if (isset($info['conflicting_range']['via'])) {
      $this->parseConflictingEntityString($info['conflicting_range']['via']);
    }
    
    $this->restriction_rule = $info['conflicting_range']['rule'];
    
    $this->conflicting_range = $info['conflicting_range'];
    $this->referring_entity_date_value = $info['referring_entity_date_value'];
    
  }
  
  /**
   * some overlaps may be from the referrer itself 
   * (e.g. entity update without changing a field).
   * if this is the case, it's not a valid conflict
   */
  public function isValid() {
    return $this->_valid === true;
  }

  /**
   * the conflicting referrer data is passed as a string: "entity_type_name:identifier"
   * this function parses that out and returns an entity
   */
  public function parseConflictingEntityString($string) {
    if ($string == 'self') {
      $this->conflicting_entity = $string;
    } else {
      $via_parts = explode(":" , $string);
      if (count($via_parts)==2) {
        //@todo: check to see if $this->referring_entity and $this->referring_entity_type properties are set
        if ($via_parts[0] == $this->referring_entity_type && $via_parts[1] == $this->referring_entity_id) {
          //@todo: what if the referrer has numerous dates in the date field and some of them overlap eachother?
          $this->_valid = false;
        } else {
          $this->_valid = true;
          $this->conflicting_entity_type = $via_parts[0];
          $this->conflicting_entity = reset(entity_load($via_parts[0], array($via_parts[1])));
          $this->conflicting_entity_id = _entityavailability_get_entity_id($this->conflicting_entity_type, $this->conflicting_entity);
        }
      }
    }
  }
  
  public function addResolutionOption(EntityAvailabilityResolution $resolution) {
    $this->_resolutions[] = $resolution;
  }
  
  /**
   * Try to get the available entity label that is shown in the entity reference widget
   * from the form state...
   * Note that $this->available_entity->label is only used as a fallback because there's a 
   * chance that the entity label might not be used directly in the entityreference widget
   * (e.g a views select is used)
   * @todo: what if this is a reference to myself... allow entity names to be overridden somehow
   */
  public function getAvailableEntityLabel() {
    if ($value = $this->getReferrerReferenceFieldValue()) {
      //@todo: can't you use a wrapper on this or something better... what if target_id isn't the key?  what if another language?
      if (isset($this->form_state['complete form'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE]['#options'][$value])) {
        return $this->form_state['complete form'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE]['#options'][$value];
      }
    }
    
    // fallback to available entity label
    if (isset($this->available_entity->label)) {
      return $this->available_entity->label;
    }
    
    // and finally fallback to "This entity"
    return t("This entity");
  }
  
  public function formErrorMessage() {
    $message =  t("!entity_label is not available during the selected time.",array("!entity_label" => $this->getAvailableEntityLabel()));
    //TODO: _optionally_ add information about the conflicting entity into the message (who owns it and what does it look like)
    $conflicter = user_load($this->conflicting_entity->uid);
    dpm($this->conflicting_entity);
    dpm($conflicter);
    if (count($this->_resolutions)>0) {
      $message .= "<br/>You have the following options:<ul>";
      foreach ($this->_resolutions as $resolution) {
        $message .= "<li>" . $resolution->getResolutionOptionsItem() . "</li>";
      }
      $message .= "</ul>";
    }
    return $message;
  }
  
  /**
   * return the restriction rule referrer date field name
   */
  public function getReferrerDateFieldName() {
    return $this->restriction_rule['referrer_to_restrict_date'];
  }
  
  /**
   * return the restriction rule referrer-to-reference entityreference field name
   */
  public function getReferrerReferenceFieldName() {
    return $this->restriction_rule['referrer_to_restrict_entityreference'];
  }
  
  /**
   * return the restriction rule referrer-to-reference entityreference field value
   * @todo: can't you use a wrapper on this or something better... what if target_id isn't the key?  what if another language?
   */
  public function getReferrerReferenceFieldValue() {
    if (isset($this->form_state['values'][$this->getReferrerReferenceFieldName()])
      && isset($this->form_state['values'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE])
      && isset($this->form_state['values'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE][0])
      && isset($this->form_state['values'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE][0]['target_id'])
    ) {
      return $this->form_state['values'][$this->getReferrerReferenceFieldName()][LANGUAGE_NONE][0]['target_id'];
    }
    return false;
  }
  
  /**
   * describe relationships between referrer and available entity
   * @see entityavailabiltiy_relationship_types()
   */
  public function referrerToAvailableEntityRelationships() {
    $relationships = array();
    if (!isset($this->referring_entity->is_new) && $this->referring_entity->identifier() == $this->available_entity->identifier() && $this->referring_entity_type == $this->available_entity_type) {
      // the referrer and the available entity are the same.
      $relationships['same_entity'] = true;
    }
    
    if ($this->referring_entity->uid == $this->available_entity->uid) {
        // the referrer user owns the available entity.
        $relationships['same_owner'] = true;
    } else {
      foreach(user_relationships_load(array('between' => array($this->referring_entity->uid, $this->available_entity->uid))) as $rid => $relationship) {
        $relationships['user_relationships'][$rid] = $relationship;
      }
    }
    return $relationships;
  }
  
  /**
   * describe relationships between referrer and conflicting entity
   * @see entityavailabiltiy_relationship_types()
   */
  public function referrerToConflictingEntityRelationships() {
    $relationships = array();
    if (!isset($this->referring_entity->is_new) && $this->referring_entity_id == $this->conflicting_entity_id && $this->referring_entity_type == $this->conflicting_entity_type) {
      // the referrer and the conflicting referrer are the same entity.
      $relationships['same_entity'] = true;
    }
    if ($this->referring_entity->uid == $this->conflicting_entity->uid) {
        // the referrer user owns the conflicting entity.
        $relationships['same_owner'] = true;
    } else {
      foreach(user_relationships_load(array('between' => array($this->referring_entity->uid, $this->conflicting_entity->uid))) as $rid =>$relationship) {
        $relationships['user_relationships'][$relationship->rtid] = $relationship;
      }
    }
    return $relationships;
  }
}