<?php

class EntityAvailabilityResolution {

  public $restriction_rule;
  public $conflict;
  
  public function __construct($restriction_rule, $conflict) {
    $this->restriction_rule = $restriction_rule;
    $this->conflict = $conflict;
  }
  
  public function getResolutionOptionsItem() {
    return "foo";
  }

}

class EntityAvailabilityResolutionDoubleBook extends EntityAvailabilityResolution {
  public function getResolutionOptionsItem() {
    return "double book";
  }
}

class EntityAvailabilityResolutionShareOwnership extends EntityAvailabilityResolution {
  public function getResolutionOptionsItem() {
    return "Take shared ownership";
  }
}